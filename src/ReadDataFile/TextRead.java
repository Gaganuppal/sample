package ReadDataFile;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;

public class TextRead {

	public static void main(String[] args) throws Exception {

		String s;
		//FileReader obj = new FileReader("C:\\Users\\Param\\Documents\\ChatLog Automation Class _10_14_2020 _1_30 PM PST 2020_10_14 17_43.txt");
		FileReader obj = new FileReader("text.txt");
		BufferedReader obj1 = new BufferedReader(obj);
		
		while((s=obj1.readLine())!=null) {
			System.out.println(s);
		}
		obj1.close();
	}

}
