package ReadDataFile;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class TextWrite {

	public static void main(String[] args) throws Exception {

		//FileWriter obj = new FileWriter("C:\\Users\\Param\\Documents\\sample.txt");
		//FileWriter obj = new FileWriter("C:\\Users\\Param\\Documents\\ChatLog Automation Class _10_14_2020 _1_30 PM PST 2020_10_14 17_43.txt");
		FileWriter obj = new FileWriter("text.txt");
		//FileWriter obj = new FileWriter("C:\\Users\\Param\\Documents\\textwrite.txt");
		BufferedWriter obj1 = new BufferedWriter(obj);
		
		obj1.write("This is a sample text.");
		obj1.newLine();

		obj1.write("Software Testing");
		obj1.newLine();
		
		//obj1.write("4");
		obj1.write("hello");
		obj1.close();
		System.out.println(new File(".").getAbsolutePath());

	}

}






