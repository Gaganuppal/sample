package LearnExcel;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadfromExcel {
	
	public void readExcel(String filepath, String filename, String sheetname) throws Exception {
		
		//creating an object of File class to open xlsx file
		File fl = new File(filepath+ "//"+ filename);
		
		//create a object of FileInputStream class to read  excel file
		FileInputStream input = new FileInputStream(fl);
		
		Workbook wb = null;
		
		//find the file extension by splitting file name in substring and getting
		String extension = filename.substring(filename.indexOf("."));
		
		//check if the file is with .xlsx extension
		if (extension.equals(".xlsx")){
			//create object of XSSFWorkbook class
			wb = new XSSFWorkbook(input);
			
		
			} else if(extension.equals(".xls")) {
				wb = new HSSFWorkbook(input);
			}
			
		//reading sheet inside the workbook by its name
		Sheet excelsheet =   wb.getSheet(sheetname);
		
		//finding number of rows in excel file
		int rowcount = excelsheet.getLastRowNum()-excelsheet.getFirstRowNum();
		
		//create a loop over all the rows of excel file to read it
		for(int i=0; i<rowcount+1; i++) {
			
			Row row = excelsheet.getRow(i);
 
			//create a loop inside to print the cell value in a row
		for (int j=0; j<row.getLastCellNum(); j++) {
			
			//print excel data in console
			 System.out.print(row.getCell(j).getStringCellValue()+"// ");
		}
			System.out.println();
		}
		}
		
		
		
	

	public static void main(String[] args) throws Exception {

		ReadfromExcel obj = new ReadfromExcel(); //created object
		
		String filepath = "C:\\Users\\Param\\Documents\\"; //created path for excel file
		//String filepath = "https://docs.google.com/spreadsheets/u/0/?authuser";
		obj.readExcel(filepath, "Sample.xlsx", "Sheet1" );
	}

	
}
