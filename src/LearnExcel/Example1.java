package LearnExcel;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Example1 {

	public static void main(String[] args) throws Exception {

		FileInputStream file = new FileInputStream("C:\\Users\\Param\\Documents\\Sample.xlsx");
		XSSFWorkbook wb = new XSSFWorkbook(file);
		XSSFSheet sheet = wb.getSheet("Sheet1");
		
		int rowcount = sheet.getLastRowNum();
		int columncount = sheet.getRow(0).getLastCellNum();
		
		for (int i=0; i<rowcount; i++) {
			
			Row row = sheet.getRow(i);
			
			for (int j=0; j<columncount; j++) {
				String value = row.getCell(j).toString();
				System.out.print("\\"+value);
			}
			
			System.out.println();
			wb.close();
		}
		
		
	}

}
