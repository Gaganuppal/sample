package LearnExcel;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class WriteinExcel {

	public void writeExcel(String filepath, String filename, String sheetname, String [] datatowrite) throws Exception {
		
		File file = new File(filepath + "//" + filename);
		FileInputStream input = new FileInputStream(file);
		Workbook wb = null;
		
		String extension = filename.substring(filename.indexOf("."));
		if (extension.equals(".xlsx")) {
			wb = new XSSFWorkbook(input);
		}
		else if (extension.equals(".xls")) {
			wb = new HSSFWorkbook(input);
		}
		
		//read excelsheet by sheetname
		Sheet sheet = wb.getSheet(sheetname);
		//get the current number of rows in excelsheet
		//int rowcount = sheet.getLastRowNum()-sheet.getFirstRowNum();
		int rowcount = sheet.getLastRowNum();
		System.out.println(rowcount);
		
		//get first row
		Row row = sheet.getRow(0);
		//create a new row at end
		Row newrow = sheet.createRow(rowcount+1);
		
		for(int j=0; j < row.getLastCellNum(); j++) {
			//fill data in row
			Cell cell = newrow.createCell(j);
			cell.setCellValue(datatowrite[j]);
		}
		//close input stream
		input.close();
		
		FileOutputStream output = new FileOutputStream(file);
		//write data
		wb.write(output);
		output.close();
	}
	public static void main(String[] args) throws Exception {
//create an array with the data
		//String [] value = {"mom", "hello", "abc123"};
		String[] value = {"yes", "no", "ok"};
		
		//create an object of current class
		WriteinExcel obj = new WriteinExcel();
		
        //Write the file using file name , sheet name and the data to be filled
obj.writeExcel("C:\\Users\\Param\\Documents\\", "Sample.xlsx", "Sheet1", value);


	}

}
