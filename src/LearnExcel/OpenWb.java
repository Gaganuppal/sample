package LearnExcel;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class OpenWb {

	public static void main(String[] args) throws Exception {

		File file = new File("C:\\Users\\Param\\Documents\\Sample.xlsx");
		FileInputStream input = new FileInputStream(file);
		
		XSSFWorkbook wb = new XSSFWorkbook(input);
		
		if(file.isFile() && file.exists()) {
	         System.out.println("Sample.xlsx file open successfully.");
	      } else {
	         System.out.println("Error to open Sample.xlsx file.");
	      }
	   }
	}


