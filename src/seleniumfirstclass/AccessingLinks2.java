package seleniumfirstclass;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class AccessingLinks2 {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\Param\\Documents\\Drivers\\chromedriver_win32 (1)\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();

		String url = "C:\\Users\\Param\\Documents\\practice.html";
		driver.get(url);
		
		WebElement obj = driver.findElement(By.xpath("/html/body/p[1]"));
		String s = obj.getText();
		System.out.println(s);
	}

}
