package seleniumfirstclass;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class RadioButton {

	public static void main(String[] args) {
System.setProperty("webdriver.gecko.driver", "C:\\Users\\Param\\Downloads\\geckodriver-v0.27.0-win64\\geckodriver.exe");

WebDriver driver = new FirefoxDriver();

driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

driver.navigate().to("https://www.calculator.net/mortgage-payoff-calculator.html");
driver.manage().window().maximize();

 driver.findElement(By.id("cpayoff1")).click();

System.out.println(driver.findElement(By.id("cpayoff1")).isSelected());
System.out.println(driver.findElement(By.id("cpayoff1")).isEnabled());
System.out.println(driver.findElement(By.id("cpayoff1")).isDisplayed());


driver.close();



	}

}
