package seleniumfirstclass;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class DropDown {

	public static void main(String[] args) {
System.setProperty("webdriver.gecko.driver", "C:\\Users\\Param\\Downloads\\geckodriver-v0.27.0-win64\\geckodriver.exe");
WebDriver driver = new FirefoxDriver();

driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

driver.navigate().to("https://www.calculator.net/interest-calculator.html");
driver.manage().window().maximize();

Select dropdown = new Select(driver.findElement(By.id("ccompound")));

//dropdown.selectByVisibleText("daily");

//dropdown.selectByIndex(1);
dropdown.selectByValue("daily");

System.out.println(driver.findElement(By.id("ccompound")).isSelected());
System.out.println(driver.findElement(By.id("ccompound")).isEnabled());
System.out.println(driver.findElement(By.id("ccompound")).isDisplayed());

driver.close();
	}

}
