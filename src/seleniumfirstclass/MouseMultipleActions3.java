package seleniumfirstclass;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;

public class MouseMultipleActions3 {

	public static void main(String[] args) {

		System.setProperty("webdriver.gecko.driver", "C:\\Users\\Param\\Downloads\\geckodriver-v0.27.0-win64\\geckodriver.exe");
		WebDriver driver = new FirefoxDriver();
		
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		String url = "http:/www.facebook.com";
		
		driver.navigate().to(url);
		driver.manage().window().maximize();
		
		Actions builder = new Actions(driver);
		WebElement username = driver.findElement(By.id("email"));
		WebElement password  = driver.findElement(By.id("pass"));
		
		Action obj = builder.moveToElement(username).click().keyDown(username, Keys.SHIFT).sendKeys(username, "Gagan").keyUp(username, Keys.SHIFT).sendKeys(username, Keys.TAB).sendKeys(password, "hello").build();
		obj.perform();
	}

}
