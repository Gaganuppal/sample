package seleniumfirstclass;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class TextBox {

	public static void main(String[] args) {
System.setProperty("webdriver.gecko.driver", "C:\\Users\\Param\\Downloads\\geckodriver-v0.27.0-win64\\geckodriver.exe");

WebDriver driver =  new FirefoxDriver();

driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS); //puts a implicit wait, will wait for 10 sec before throwing exceptions

driver.navigate().to("https://www.calculator.net/percent-calculator.html");  //launch website

driver.manage().window().maximize();             // maximize browser

driver.findElement( By.id("cpar1")).sendKeys("10"); //enter value of 10

String result = driver.findElement(By.id("cpar1")).getAttribute("value");

System.out.println(result);

driver.close();  //close the browser
	}

}
