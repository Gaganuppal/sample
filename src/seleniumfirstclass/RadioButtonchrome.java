package seleniumfirstclass;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.*;

public class RadioButtonchrome {

	public static void main(String[] args) {
System.setProperty("webdriver.chrome.driver", "C:\\Users\\Param\\Documents\\Drivers\\chromedriver_win32 (1)\\chromedriver.exe");
WebDriver driver = new ChromeDriver();
driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

driver.navigate().to("https://www.calculator.net/mortgage-payoff-calculator.html");
driver.manage().window().maximize();

driver.findElement(By.id("cpayoff3")).click();

System.out.println(driver.findElement(By.id("cpayoff3")).isSelected());
System.out.println(driver.findElement(By.id("cpayoff3")).isEnabled());
System.out.println(driver.findElement(By.id("cpayoff3")).isDisplayed());

driver.close();
	}

}
