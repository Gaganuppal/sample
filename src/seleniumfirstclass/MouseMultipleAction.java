package seleniumfirstclass;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;

public class MouseMultipleAction {

	public static void main(String[] args) throws InterruptedException {

		System.setProperty("webdriver.gecko.driver", "C:\\Users\\Param\\Downloads\\geckodriver-v0.27.0-win64\\geckodriver.exe");
		WebDriver driver = new FirefoxDriver();
		
		driver.get("http://www.facebook.com");
		
		Thread.sleep(2000);
		
		WebElement obj = driver.findElement(By.id("email"));
		obj.sendKeys("");
		

		
		
		Actions builder = new Actions(driver);
		Action seriesofAction = builder.moveToElement(obj).click().keyDown(obj, Keys.SHIFT).sendKeys(obj, "hello").keyUp(obj, Keys.SHIFT).doubleClick(obj).contextClick().build();
		
		seriesofAction.perform();
		
		
	}

}
