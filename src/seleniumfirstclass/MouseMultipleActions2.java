package seleniumfirstclass;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;

import java.util.List;


public class MouseMultipleActions2 {

	public static void main(String[] args) throws InterruptedException {

		System.setProperty("webdriver.gecko.driver", "C:\\Users\\Param\\Downloads\\geckodriver-v0.27.0-win64\\geckodriver.exe");
		WebDriver driver = new FirefoxDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		driver.navigate().to("https://demos.devexpress.com/aspxeditorsdemos/ListEditors/MultiSelect.aspx");
		driver.manage().window().maximize();
		
		driver.findElement(By.id("ControlOptionsTopHolder_lbSelectionMode_I")).click();
		driver.findElement(By.id("ControlOptionsTopHolder_lbSelectionMode_DDD_L_LBI1T0")).click();
		
		
		Thread.sleep(5000);
		
		//perform multiple select
		Actions builder = new Actions(driver);
		
		WebElement select = driver.findElement(By.id("ContentHolder_lbFeatures_LBT"));
		//WebElement select = driver.findElement(By.id("ContentHolder_lbFeatures"));

		List<WebElement> options = select.findElements(By.tagName("td"));
		
		System.out.println(options.size()); // int x = options.size();
		
		//Action multipleselect = builder.keyDown(Keys.CONTROL).click(options.get(1)).click(options.get(4)).build();
	//	multipleselect.perform();
		
		Action multipleselect = builder.keyDown(Keys.COMMAND).click(options.get(1)).click(options.get(4)).keyUp(Keys.COMMAND).build();
		multipleselect.perform();
		
		
		
		//Action multipleselect = builder.keyDown(Keys.SHIFT).click(options.get(1)).click(options.get(2)).build();
		//multipleselect.perform();
		
		
	}

}
